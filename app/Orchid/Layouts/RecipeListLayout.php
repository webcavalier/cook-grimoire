<?php

namespace App\Orchid\Layouts;

use App\Models\Recipe;
use Orchid\Screen\TD;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Layouts\Table;
use Orchid\Support\Facades\Layout;

class RecipeListLayout extends Table
{
    /**
     * Data source.
     *
     * @var string
     */
    public $target = 'recipes';

    /**
     * @return TD[]
     */
    public function columns(): array
    {
        return [
            TD::make('hero', 'Image')
                ->width('150')
                ->render(fn (Recipe $recipe) => // Please use view('path')
                "<img src='{$recipe->hero}' style='max-width: 100px; max-height: 100px'>"),
            TD::make('name', 'Nom')
                ->render(function (Recipe $recipe) {
                    return Link::make($recipe->name)
                        ->route('platform.recipe.edit', $recipe);
                }),
        ];
    }
}
