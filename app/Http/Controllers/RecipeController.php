<?php

namespace App\Http\Controllers;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Carbon\Carbon;

class RecipeController extends Controller
{
    public function index() {
        $recipes = Recipe::inRandomOrder()
            ->get();
        return view('recipes.index', compact('recipes'));
    }

    public function list() {
        $recipes = Recipe::inRandomOrder()
        ->take(10)
        ->get();
        foreach ($recipes as $recipe) {
            $timeCook = Carbon::parse($recipe->cook);
            $timePrep = Carbon::parse($recipe->prep);
            $endTime = $timeCook->addMinutes($timePrep);
            $allocateValidateMessage = Carbon::parse($recipe->cook)->format('H:i') . ' - ' . $timePrep->format('H:i') . ' - ' . $endTime->format('H:i');
        }
        
        return view('home', compact('recipes'));
    }
}
