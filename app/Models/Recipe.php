<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Orchid\Screen\AsSource;

class Recipe extends Model
{
    use AsSource;

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'prep',
        'cook',
        'portion',
        'ingredient',
        'step',
        'tags',
        'hero'
    ];

    protected $casts = [
        'ingredient' => 'array',
        'step' => 'array',
        'tags' => 'array',
    ];
}