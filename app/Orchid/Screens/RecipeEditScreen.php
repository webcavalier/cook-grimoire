<?php

namespace App\Orchid\Screens;

use App\Models\Recipe;
use Illuminate\Http\Request;
use Orchid\Screen\Fields\Input;
use Orchid\Screen\Fields\TextArea;
use Orchid\Screen\Fields\Select;
use Orchid\Screen\Fields\Matrix;
use Orchid\Screen\Fields\Picture;
use Orchid\Screen\Fields\Group;
use Orchid\Support\Facades\Layout;
use Orchid\Screen\Actions\Button;
use Orchid\Screen\Screen;
use Orchid\Support\Facades\Alert;
use Orchid\Support\Color;

class RecipeEditScreen extends Screen
{
    /**
     * @var Recipe
     */
    public $recipe;

    /**
     * Query data.
     *
     * @param Recipe $recipe
     *
     * @return array
     */
    public function query(Recipe $recipe): array
    {   
        return [
            'recipe' => $recipe
        ];
    }

    /**
     * The name is displayed on the user's screen and in the headers
     */
    public function name(): ?string
    {
        return $this->recipe->exists ? 'Modifier la recette : ' . $this->recipe->name : 'Crééer une recette';
    }

    /**
     * The description is displayed on the user's screen under the heading
     */
    public function description(): ?string
    {
        return "Hummmm ca va être trop bon !";
    }

    /**
     * Button commands.
     *
     * @return Link[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            Layout::rows([
                Input::make('recipe.name')
                    ->title('Nom')
                    ->required()
                    ->placeholder('Potato chibre'),

                Input::make('recipe.portion')
                    ->title('Portion')
                    ->type('number')
                    ->required()
                    ->help('Le nombre de portion (peut être des personnes, des parts...'),

                Group::make([
                    Input::make('recipe.prep')
                        ->title('Temps de préparation')
                        ->required()
                        ->type('time'),

                    Input::make('recipe.cook')
                        ->title('Temps de cuisson')
                        ->required()
                        ->type('time'),
                ]),

                Matrix::make('recipe.ingredient')
                    ->title('Liste des ingrédients')
                    ->required()
                    ->columns([
                        'Ingrédient' => 'name',
                        'Quantité' => 'quantity',
                        'Unité' => 'unity',
                    ])
                    ->fields([
                        'name' => Input::make()->type('string'),
                        'quantity'   => Input::make()->type('number'),
                        'unity'   => Select::make('select')
                            ->options([
                                'piece' => 'pièces',
                                'gramme'   => 'grammes',
                                'centiliter' => 'centilitres',
                                'liter' => 'litres',
                                'tableSpoon' => 'c à s',
                                'teaSpoon' => 'c à c',
                                'pinch' => 'pincées',
                            ]),
                    ]),

                Matrix::make('recipe.step')
                    ->title('Liste des étapes')
                    ->required()
                    ->columns([
                        'Etape' => 'step',
                    ])
                    ->fields([
                        'step' => TextArea::make()
                            ->rows(50),
                    ]),

                Matrix::make('recipe.tags')
                    ->title('Liste des tags')
                    ->columns([
                        'Tag' => 'tags',
                    ]),

                Picture::make('recipe.hero')
                ->storage('public'),

            ]),
            Layout::rows([
                Group::make([
                    Button::make('Créer la recette')
                        ->icon('pencil')
                        ->method('createOrUpdate')
                        ->canSee(!$this->recipe->exists)
                        ->type(Color::SUCCESS()),

                    Button::make('Mettre à jour')
                        ->icon('note')
                        ->method('createOrUpdate')
                        ->canSee($this->recipe->exists)
                        ->type(Color::SUCCESS()),

                    Button::make('Supprimer')
                        ->icon('trash')
                        ->method('remove')
                        ->canSee($this->recipe->exists)
                        ->type(Color::DANGER()),
                ])->autoWidth(),
            ]),
        ];
    }

    /**
     * @param Recipe    $recipe
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createOrUpdate(Recipe $recipe, Request $request)
    {
        $recipe->fill($request->get('recipe'))->save();
        
        Alert::info('La recette a bien été mise à jour / créer.');

        return redirect()->route('platform.recipe.list');
    }

    /**
     * @param Recipe $recipe
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function remove(Recipe $recipe)
    {
        $recipe->delete();

        Alert::info('You have successfully deleted the recipe.');

        return redirect()->route('platform.recipe.list');
    }
}
