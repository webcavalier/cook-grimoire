<a href="#" class="recipe-container">
    <div class="recipe">
        <div class="recipe-image" style="background-image: url('{{$hero}}');"></div>
        <div class="recipe-info">
            <div class="recipe-info-header">
                <h3 class="recipe-title">{{ $name }}</h3>
                <div class="recipe-time">
                    <i class="fa-solid fa-clock"></i>
                    
                    
                </div>
            </div>
            <div class="tag-container">
                @foreach ($tags as $tag)
                    <span class="tag">{{ $tag['tags'] }}</span>
                @endforeach
            </div>
        </div>
    </div>
</a>
