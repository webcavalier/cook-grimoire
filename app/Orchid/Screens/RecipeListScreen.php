<?php

namespace App\Orchid\Screens;

use App\Orchid\Layouts\RecipeListLayout;
use App\Models\Recipe;
use Orchid\Screen\Actions\Link;
use Orchid\Screen\Screen;

class RecipeListScreen extends Screen
{
    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return [
            'recipes' => Recipe::paginate()
        ];
    }

    /**
     * The name is displayed on the user's screen and in the headers
     */
    public function name(): ?string
    {
        return 'Recettes';
    }

    /**
     * The description is displayed on the user's screen under the heading
     */
    public function description(): ?string
    {
        return "Toutes les recettes.";
    }

    /**
     * Button commands.
     *
     * @return Link[]
     */
    public function commandBar(): array
    {
        return [
            Link::make('Ajouter une recette')
                ->icon('pencil')
                ->route('platform.recipe.edit')
        ];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [
            RecipeListLayout::class
        ];
    }
}