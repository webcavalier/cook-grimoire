<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Styles/Script -->
        <script src="https://kit.fontawesome.com/f285c1b662.js" crossorigin="anonymous"></script>
        @vite(['resources/sass/app.scss', 'resources/js/app.js'])
    </head>
    <body class="antialiased">
        <section class="container">
            <div class="header">
                <h2>Recettes</h2>
                <a href="#">Tout voir</a>
            </div>
            <div class="list-container">
                @foreach($recipes as $recipe)
                    <x-recipe-card :name="$recipe['name']" :hero="$recipe['hero']" :tags="$recipe['tags']" :cook="$recipe['cook']" :prep="$recipe['prep']"></x-recipe-card>
                @endforeach
            </div>
        </section>
    </body>
</html>
